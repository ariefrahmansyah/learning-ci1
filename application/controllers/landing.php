<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends CI_Controller {

	private $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->data['judulhalaman'] = "Landing";
	}	

	public function index()
	{
		$this->load->view('landing', $this->data);
	}

	public function hello($nama = "")
	{
		$this->data['nama'] = $nama;

		$this->load->view('landing', $this->data);
	}
}